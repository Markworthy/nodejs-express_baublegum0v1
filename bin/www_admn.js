#!/usr/bin/env node

const plib= require('./port_lib');
plib.renderString= plib.revealString;

const app= require('../app');
const debug= require('debug')('demo:server');
const http= require('http');

const port= plib.normalizePort(process.env.PORT || '3330');
app.set('port', port);

const server= http.createServer(app);

server.on('error', plib.onError_(port));
server.on('listening', plib.onListening_(server, debug));
if (0===plib.countEnv('RENDER_')) {}
else server.listen(port);

//
// OUTOUTOUT:=
//

/**
 * Normalize a port into a number, string, or false.
 */

function out_normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function out_onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function out_onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  console.log('www.js (conlog): Listening on ' + bind);
  debug('www.js: Listening on ' + bind);
}
