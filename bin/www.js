#!/usr/bin/env node

const plib= require('./port_lib');
//plib.renderString= plib.revealString;

const app= require('../app');
const debug= require('debug')('demo:server');
const http= require('http');

const port= plib.normalizePort(process.env.PORT || '3300');
app.set('port', port);

const server= http.createServer(app);

server.on('error', plib.onError_(port));
server.on('listening', plib.onListening_(server, debug));
if (0===plib.countEnv('RENDER_')) {}
else server.listen(port);
